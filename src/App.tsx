import React from 'react';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
import { Routes } from './Routes';

const App = () => (
  <div className="wrapper">
    <Router>
      <Routes />
    </Router>
  </div>
)

export default App;
