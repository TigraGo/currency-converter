import React from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { usePaymentMethods, API } from '../../services/api'
import { ConfirmationPage } from './ConfirmationPage'

export const ConfirmationPageContainer = () => {
  const {response} = usePaymentMethods('payMethods', {})
  const {base, amountinvoice, amountwithdraw, invoicepaymethod, withdrawpaymethod} = useParams()
  const history = useHistory()
  
  if (!response) return <div>Loading...</div>
  const invoicePayMethodName = response.invoice.filter((item: any) => item.id === Number(invoicepaymethod))
  const withdrawPayMethodName = response.withdraw.filter((item: any) => item.id === Number(withdrawpaymethod))
  
  const createOrderForExchange = async (amount: number, base: string, invoicePayMethod: number, withdrawPayMethod: number) => {
    const requestData = {
        amount: Number(amount),
        base,
        invoicePayMethod: Number(invoicePayMethod),
        withdrawPayMethod: Number(withdrawPayMethod)
    }
  
    const res = await API('/bids', 'POST', requestData)
    const json = await res.json()
  
    json.message === 'Success' ? history.push('/success') : console.log('something wrong')
  }

  const confirmationPageProps = {
    base,
    amountinvoice,
    amountwithdraw,
    invoicepaymethod,
    withdrawpaymethod,
    invoicePayMethodName,
    withdrawPayMethodName,
    createOrderForExchange
  }

  return <ConfirmationPage confirmationPageProps={confirmationPageProps} />
}
