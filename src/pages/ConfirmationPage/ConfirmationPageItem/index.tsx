import React, {FC} from 'react'
import { ConfirmationItem } from "./styles"

interface ConfirmationPageItemProps {
  title: string
  amount: number
  paymentMethodName: string
}

export const ConfirmationPageItem:FC<ConfirmationPageItemProps> = ({ title, amount, paymentMethodName }) => (
  <ConfirmationItem>
    <span>{title}</span>
    <span>{amount} {paymentMethodName}</span>
  </ConfirmationItem>
)