import styled from '@emotion/styled'

export const ConfirmationItem = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 35px;
  
  span {
    color: #818181;
    font-size: 16px;
    line-height: 1;

    &:last-of-type {
      color: #3A3A3A;
    }
  }
`