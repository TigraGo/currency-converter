import React, {FC} from 'react'
import { useHistory } from 'react-router-dom'
import { ConfirmationPageItem } from './ConfirmationPageItem'
import { Title, PrimaryButton, CancelButton } from '../../components/styles.base'
import { ConfirmationPageWrapper, ActionButtonsWrapper } from './styles'

interface ConfirmationPageProps {
  confirmationPageProps: {
    base: string
    invoicepaymethod: number
    withdrawpaymethod: number
    amountinvoice: number
    amountwithdraw: number
    invoicePayMethodName: [{
      id: number
      name: string
    }]
    withdrawPayMethodName: [{
      id: number
      name: string
    }]
    createOrderForExchange: (amountinvoice: number, base: string, invoicepaymethod: number, withdrawpaymethod: number) => void
  }
}

export const ConfirmationPage:FC<ConfirmationPageProps> = ({confirmationPageProps}) => {
  const history = useHistory()  
  const {base, amountinvoice, amountwithdraw, invoicePayMethodName, withdrawPayMethodName, invoicepaymethod, withdrawpaymethod, createOrderForExchange} = confirmationPageProps
  return (
    <>
      <div>
        <Title>Details</Title>
        <ConfirmationPageWrapper style={{width: '460px'}}>
          <ConfirmationPageItem title="Sell" amount={amountinvoice} paymentMethodName={invoicePayMethodName[0].name} />
          <ConfirmationPageItem title="Buy" amount={amountwithdraw} paymentMethodName={withdrawPayMethodName[0].name} />
        </ConfirmationPageWrapper>
        <ActionButtonsWrapper>
          <CancelButton onClick={() => history.push('/')}>Cancel</CancelButton>
          <PrimaryButton onClick={() => createOrderForExchange(amountinvoice, base, invoicepaymethod, withdrawpaymethod)}>Confirm</PrimaryButton>
        </ActionButtonsWrapper>
      </div>
    </>
  )
}