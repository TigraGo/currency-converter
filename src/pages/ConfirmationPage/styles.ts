import styled from '@emotion/styled'

export const ConfirmationPageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-width: 460px;
  width: 100%;
`

export const ActionButtonsWrapper = styled.div`
  display: flex;
  max-width: 300px;
  width: 100%;
  margin: 0 auto;
`