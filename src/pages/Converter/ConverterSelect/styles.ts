import {Select} from 'antd'
import styled from '@emotion/styled'

export const {Option} = Select
export const ConverterSelectWrapper = styled(Select)`
  .ant-select-selector {
    height: 46px !important;
    font-weight: bold;

    input {
      height: calc(100% - 2px) !important;
    }
  }

  .ant-select-selection-item {
    line-height: 44px !important;
    font-size: 18px;
    font-weight: bold;
    color: #3a3a3a;
  }

  .ant-select-item {
    font-weight: bold;
    font-size: 18px;
    color: #3a3a3a;
  }
`