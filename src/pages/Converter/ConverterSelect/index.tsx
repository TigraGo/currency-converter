import React, {FC} from 'react'
import { ConverterSelectWrapper, Option } from './styles'
import { Title } from '../../../components/styles.base'

interface ConverterSelectProps {
  title: string
  selectOptionsData: any[]
  defaultValue?: string
  selectWidth?: number
  onChange: (e: any) => void
}

export const ConverterSelect:FC<ConverterSelectProps> = ({title, selectOptionsData = [], defaultValue, selectWidth = 313, onChange}) => (
  <>
    <Title>{title}</Title>
    <ConverterSelectWrapper defaultValue={defaultValue} style={{width: selectWidth}} onChange={onChange}>
      {selectOptionsData.map((item: {id: number, name: string})=> (
        <Option style={{fontSize:'18px', fontWeight: 'bold', color: '#3a3a3a', padding: '10px 12px'}} key={item.id} value={item.id}>{item.name}</Option>
      ))}
    </ConverterSelectWrapper>
  </>
)