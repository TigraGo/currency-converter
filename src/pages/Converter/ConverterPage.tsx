import React, {FC} from 'react'
import { ConverterSelect } from './ConverterSelect'
import { ConverterInput } from './ConverterInput'
import { ConverterWrapper, ConverterItem } from './styles'
import { PrimaryButton } from '../../components/styles.base'

interface PayMethod {
  id: number;
  name: string;
 }
 
interface ConverterPageProps {
  converterPageProps: {
    response: {
      invoice: PayMethod[]
      withdraw: PayMethod[]
    }
    inputValue: {
      invoice: number | any
      withdraw: number | any
    }
    base: string
    paymentMethodId: number | any
    changePaymentMethodId: (e: any, base: string) => void
    submitExchange: (base: string, inputValue: any, paymentMethodId: number) => void
    debouncedCalculateAmount: (e: any, base: string) => void
  }
}

export const ConverterPage:FC<ConverterPageProps> = ({converterPageProps}) => {

  const {response, inputValue, base, paymentMethodId, changePaymentMethodId, submitExchange, debouncedCalculateAmount} = converterPageProps
  return (
    <div>
    <ConverterWrapper>
      <ConverterItem>
        <ConverterSelect title="Sell" selectOptionsData={response.invoice} defaultValue="Choose method" onChange={(e) => changePaymentMethodId(e, 'invoice')} />
        <ConverterInput value={inputValue.invoice} onChange={(e) => debouncedCalculateAmount(e, 'invoice')} />
      </ConverterItem>

      <ConverterItem>
        <ConverterSelect title="Buy" selectOptionsData={response.withdraw} defaultValue="Choose method" onChange={(e) => changePaymentMethodId(e, 'withdraw')} />
        <ConverterInput value={inputValue.withdraw} onChange={(e) => debouncedCalculateAmount(e, 'withdraw')} />
      </ConverterItem>
    </ConverterWrapper>
    <PrimaryButton 
      disabled={(inputValue.invoice === 0 || isNaN(inputValue.invoice)) || (inputValue.withdraw === 0 || isNaN(inputValue.withdraw))} 
      onClick={() => submitExchange(base, inputValue, paymentMethodId)} type="primary">
        Exchange
    </PrimaryButton>
    </div>
  )
}