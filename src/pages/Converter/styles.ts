import styled from '@emotion/styled'

export const ConverterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const ConverterItem = styled.div`
  &:first-of-type {
    margin-right: 64px;
  }
`