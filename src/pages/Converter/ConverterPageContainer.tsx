import React, {useState} from 'react'
import { useHistory } from 'react-router-dom'
import {debounce} from 'lodash'
import { usePaymentMethods, API} from '../../services/api'
import { ConverterPage } from './ConverterPage'

export const ConverterPageContainer = () => {
  const [paymentMethodId, setPaymentMethodId] = useState({
    invoice: null,
    withdraw: null
  })
  
  const [inputValue, setInputValue] = useState({
    invoice: 0,
    withdraw: 0
  })
  
  const [base, setBase] = useState('')
  const history = useHistory()
  const {response} = usePaymentMethods('payMethods', {})
  
  if (!response) return <div>Loading...</div>
  
  const calculateAmount = async (e: any, base: string) => {
    const queryStringBody: any = {
      base,
      amount: e,
      invoicePayMethod: paymentMethodId['invoice'],
      withdrawPayMethod: paymentMethodId['withdraw']
    } // creating the object for building query
  
    const query = Object.keys(queryStringBody).map(key => key + '=' + queryStringBody[key]).join('&'); // building query
    const res = await API(`/payMethods/calculate?${query}`) // sending the request for every input's change for calculate results
    const json = await res.json();
    
    if (base === 'invoice') { //setting amount for each base cases
      setInputValue({
        invoice: queryStringBody.amount,
        withdraw: json.amount
      })
      setBase('invoice')
    } else {
      setInputValue({
        invoice: json.amount,
        withdraw: queryStringBody.amount
      })
      setBase('withdraw')
    }
  }
  
  const debouncedCalculateAmount = debounce(calculateAmount, 150) // using debounce to avoid unnecessary requests
  
  const changePaymentMethodId = (e: number, base: string) => { // set paymentId for usage in confirmation page
    setPaymentMethodId({
      ...paymentMethodId,
      [base]: e
    })
  }
  
  const submitExchange = (base: string, amounts: any, paymentMethodId: any) => { // 'exchange' button handler
    if (amounts && paymentMethodId) {
      const exchangeInfo = { // creating the object for sending data as params in url
        amounts,
        invoicePayMethod: paymentMethodId['invoice'],
        withdrawPayMethod: paymentMethodId['withdraw']
      }
      const {invoicePayMethod, withdrawPayMethod} = exchangeInfo

      history.push(`/confirmation/${base}/${exchangeInfo.amounts.invoice}-${exchangeInfo.amounts.withdraw}/${invoicePayMethod}/${withdrawPayMethod}`)
    }
    else console.log('something wrong')
  }

  const converterPageProps = { // creation of unit object for whole props (props collection pattern)
    response,
    inputValue,
    paymentMethodId,
    base,
    debouncedCalculateAmount,
    changePaymentMethodId,
    submitExchange
  }

  return <ConverterPage converterPageProps={converterPageProps} />
}