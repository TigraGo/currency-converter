import React, {FC} from 'react'
import {ConverterInputWrapper} from './styles'

interface ConverterInputProps {
  width?: number
  value: number
  onChange: (e: any) => void
}


export const ConverterInput:FC<ConverterInputProps> = ({width = 313, value, onChange}) => {

  return (
    <ConverterInputWrapper
      style={{width: width}}
      formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
      parser={value => value!.replace(/\$\s?|(,*)/g, '')}
      value={value}
      onChange={onChange}
    />
  )
}