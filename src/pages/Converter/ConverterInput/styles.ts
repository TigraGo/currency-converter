import {InputNumber} from 'antd'
import styled from '@emotion/styled'

export const ConverterInputWrapper = styled(InputNumber)`
  margin: 20px auto 35px;
  border: none;
  
  input {
    padding: 20px 12px;
    border: 1px solid #CBCBCB;
    border-radius: 4px;
  }
`