import styled from '@emotion/styled'

export const SuccessPageWrapper = styled.div`
  text-align: center;
  max-width: 460px !important;
  
  p {
    line-height: 1.5;
    text-align: center;
    padding: 0 20px;
  }
`