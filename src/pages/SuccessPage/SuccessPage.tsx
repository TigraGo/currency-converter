import React from 'react'
import {Link} from 'react-router-dom'
import {ReactComponent as SuccessLogo} from './img/success-icon.svg'
import { SubTitle, PrimaryButton } from '../../components/styles.base'
import {SuccessPageWrapper} from './styles'
export const SuccessPage = () => (
  <SuccessPageWrapper>
    <SuccessLogo />
    <SubTitle>Success!</SubTitle>
    <p>Your exchange order has been placed successfully and will be processed soon.</p>
    <PrimaryButton>
      <Link to="/">Home</Link>
    </PrimaryButton>    
  </SuccessPageWrapper>
)