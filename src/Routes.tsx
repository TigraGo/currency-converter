import React from 'react'
import {Switch, Route} from 'react-router-dom'
import { ConverterPageContainer } from './pages/Converter/ConverterPageContainer'
import { ConfirmationPageContainer } from './pages/ConfirmationPage/ConfirmationPageContainer'
import { SuccessPage } from './pages/SuccessPage/SuccessPage'

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={ConverterPageContainer} />
    <Route path="/confirmation/:base/:amountinvoice-:amountwithdraw/:invoicepaymethod/:withdrawpaymethod" component={ConfirmationPageContainer} />
    <Route path="/success" component={SuccessPage} />
  </Switch>
)