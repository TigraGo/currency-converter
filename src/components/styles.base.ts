import styled from '@emotion/styled'
import { Button } from 'antd'

export const Title = styled.h2`
  font-size: 48px;
  margin: 0 0 35px;
  font-weight: bold;
`

export const SubTitle = styled.div`
  font-size: 28px;
  font-weight: bold;
  margin: 0 0 8px;
`

export const PrimaryButton = styled(Button)`
  background-color: #58B4AE;
  border-radius: 4px;
  border-color: transparent;
  box-shadow: none;
  margin: 10px auto 0;
  display: block;
  color: #fff;
  width: 125px;
  height: 48px;
`

export const CancelButton = styled(Button)`
  border: 1px solid #58B4AE;
  box-shadow: none;
  background-color: #fff;
  margin: 10px auto 0;
  display: block;
  color: #3a3a3a;
  width: 125px;
  height: 48px;
`