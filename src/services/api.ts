import {useState, useEffect} from 'react'
export const BASE_URL: string = 'https://involve-it.com/test_front/api'


export const usePaymentMethods = (url: string, options: any) => {
  const [response, setResponse] = useState<any>(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(`${BASE_URL}/${url}`, options);
        const json = await res.json();
        setResponse(json);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, []);
  return { response, error };
};

export const API = (url: string, method?: string, body?: any) => {
  return fetch(`${BASE_URL}${url}`, {
    method,
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(body)
  })
}